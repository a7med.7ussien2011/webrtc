var express = require('express');
var app = express();
var serverPort = (process.env.PORT || 4443);
var http = require('http');
var server = http.createServer(app);

var io = require('socket.io')(server);

var roomList = {};

app.get('/', function (req, res) {
  //console.log('get /');
  res.sendFile(__dirname + '/index.html');
});
server.listen(serverPort, function () {
  //console.log('server up and running at %s port', serverPort);
});

function socketIdsInRoom(name) {
  var socketIds = io.sockets.sockets;
  if (socketIds) {
    return Object.keys(socketIds);
  } else {
    return [];
  }
}

io.on('connection', function (socket) {
  //console.log('1-connection socket id muhammad =', socket.id);

  socket.on('disconnect', function () {
    //console.log('disconnect');
    if (socket.room) {
      var room = socket.room;
      //console.log(`20- muhammad socket ${socket.id} leave room = ${room} `);
      io.to(room).emit('leave', socket.id);
      socket.leave(room, err => console.log(err));
    }
  });

  socket.on('join', function (name, callback) {
    //console.log('join', name);
    var socketIds = socketIdsInRoom(name);
    //console.log('2-roomId', name);
    //console.log('3-socketIds if room exists muhammad=', socketIds);
    callback(socketIds);
    socket.join(name);
    //console.log(`5-socket = ${socket.id} joined the room ${name} muhammad`);
    socket.room = name;
  });

  socket.on('exchange', function (data) {
    //console.log('9- muhammad exchange', data);
    data.from = socket.id;

    var to = io.sockets.sockets[data.to];
    to.emit('exchange', data);
  });
});
